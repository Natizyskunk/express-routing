'use strict';

function index (req, res) {
    res.render('home/index', {
        title: 'Home'
    });
}

function info (req, res) {
    res.render('home/info', {
        title: 'More info'
    });
}

function user (req, res) {
    res.render('home/user', {
        title: 'user infos'
    });
}

module.exports = {
    index: index,
    info: info,
    user: user
};