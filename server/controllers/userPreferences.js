'use strict';

function index (req, res) {
    res.render('userPreferences/index', {
        title: 'User Preferences'
    });
}

module.exports = {
    index: index
};