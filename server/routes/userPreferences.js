'use strict';

const
    express = require('express'),
    userPreferencesController = require('../controllers/userPreferences');

let router = express.Router();

router.get('/', userPreferencesController.index);

module.exports = router;