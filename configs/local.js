'use strict';

let localConfig = {
    hostname: 'natizyskunk-express-routing.herokuapp.com',
    port: 5000,
    viewDir: './app/views'
};

module.exports = localConfig;